from flask import Flask
from flask import render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
Bootstrap(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/gastronomia')
def gastronomia():
    return render_template('gastronomia.html')

@app.route('/fiestas')
def fiestas():
    return render_template('fiestas.html')

@app.route('/playas')
def playas():
    return render_template('playas.html')

@app.route('/lugares')
def lugares():
    return render_template('lugares.html')

@app.route('/hospedaje')
def hospedaje():
    return render_template('hospedaje.html')

if __name__ == '__main__':
    app.run()
